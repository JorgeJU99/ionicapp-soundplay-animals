/* eslint-disable @typescript-eslint/dot-notation */
import { Component } from '@angular/core';
import { Platform, AlertController } from '@ionic/angular';
import { Location } from '@angular/common';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  public labels = ['Family, Friends', 'Notes', 'Work', 'Travel', 'Reminders'];
  constructor(
    private platform: Platform,
    private location: Location,
    public alertController: AlertController
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.backButton.subscribeWithPriority(10, (processNextHandler) => {
      console.log('Back press handler!');
      if (this.location.isCurrentPathEqualTo('/home')) {
        this.showExitConfirm();
        processNextHandler();
      } else {
        this.location.back();
      }
    });
  }

  async openAlert() {
    const alert = await this.alertController.create({});
    await alert.present();
  }

  showExitConfirm() {
    this.alertController
      .create({
        header: 'Salir',
        message: 'Desea salir de la aplicación?',
        backdropDismiss: false,
        buttons: [
          {
            text: 'Cancelar',
            role: 'cancel',
          },
          {
            text: 'Salir',
            handler: () => {
              navigator['app'].exitApp();
            },
          },
        ],
      })
      .then((alert) => {
        alert.present();
      });
  }
}
