export interface Animal {
  nombre: string;
  nombreIngles: string;
  imagen: string;
  audio: string;
  duracion: number;
  reproduciendo: boolean;
}
